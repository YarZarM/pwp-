# Meetings notes

## Meeting 1.
* **DATE:** 12.02.2020
* **ASSISTANTS:** Iván Sanchez Milara

### Minutes
The wiki page needs to be improved with the details provided in the Action Points. The project is progressing smoothly and no improvements required there.

### Action points
Improvements in Section 1: 
- Explain pwa
- Describe play store api briefly in first section
- Avoid Technical details in first section
- Don't mention words like storefront because it tends towards client side
- Examples of categories
- Mention that web app details will be exposed in the API

Improvements in Section 2:
- Make a class diagram instead of a Requirements Diagram

Improvements in Section 3:
- Mention potential customers that might be interested in our API

Improvements in Section 4:
- Add link to google play publishing API
- Specify some API calls for the google play API
- Check if the SDK is an API or an RPC

### Comments from staff
Overall, the project is progressing smoothly. A few improvements need to be done in the project planning.

## Meeting 2.
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 3.
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 4.
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Midterm meeting
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Final meeting
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

